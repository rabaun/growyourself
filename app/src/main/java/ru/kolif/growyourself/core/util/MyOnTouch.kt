package ru.kolif.growyourself.core.util

import android.view.MotionEvent
import android.view.View

class MyOnTouch {
    private var xDelta: Int = 0
    private var yDelta: Int = 0

    val touchListener = View.OnTouchListener { view: View, event: MotionEvent ->

        when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                xDelta = (view.x - event.rawX).toInt()
                yDelta = (view.y - event.rawY).toInt()
            }
            MotionEvent.ACTION_MOVE -> view.animate()
                .x(event.rawX + xDelta)
                .y(event.rawY + yDelta)
                .setDuration(0)
                .start()
            MotionEvent.ACTION_UP -> {
            }
            else -> return@OnTouchListener false

        }
        view.invalidate()
        true
    }
}