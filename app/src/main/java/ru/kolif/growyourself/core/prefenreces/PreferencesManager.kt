package ru.kolif.growyourself.core.prefenreces

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by BArtWell on 10.07.2019.
 */

class PreferencesManager(context: Context) {

    val applicationPreferences: ApplicationPreferences

    init {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        applicationPreferences = ApplicationPreferences(context,preferences)
    }
}