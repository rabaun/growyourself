package ru.kolif.growyourself.core.util

import android.media.MediaPlayer
import android.view.View
import android.widget.Button
import ru.kolif.growyourself.R

var buttonPlayStop: Button? = null
 var mediaPlayer: MediaPlayer? = null

class MediaPlayer11 {
    fun initViews(v: View?) {
//        mediaPlayer = MediaPlayer.create(this, R.raw.imagine_dragons_radioactive)
        if (buttonPlayStop!!.text === R.string.play_str.toString()) {
            buttonPlayStop!!.text = R.string.pause_str.toString()
            try {
                mediaPlayer!!.start()
            } catch (e: IllegalStateException) {
                mediaPlayer!!.pause()
            }
        } else {
            buttonPlayStop!!.text = R.string.play_str.toString()
            mediaPlayer!!.pause()
        }
    }
}