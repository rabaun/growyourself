package ru.victimizer.game.core.platform

/**
 * Created by BArtWell on 10.07.2019.
 */

sealed class Failure(val throwable: Throwable?) {
    class NetworkConnection() : Failure(null)
    class ServerError(throwable: Throwable) : Failure(throwable)
    class UnknownError(throwable: Throwable) : Failure(throwable)

    abstract class FeatureFailure(throwable: Throwable) : Failure(throwable)
}
