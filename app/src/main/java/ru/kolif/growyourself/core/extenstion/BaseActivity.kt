package ru.kolif.growyourself.core.extenstion

import androidx.appcompat.app.AppCompatActivity
import ru.kolif.growyourself.R

abstract class BaseActivity : AppCompatActivity() {

    fun setupSimpleToolbar(toolbar: androidx.appcompat.widget.Toolbar, title: String) {
        toolbar.setNavigationIcon(R.drawable.ic_toolbar_back)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.title = title
    }
}