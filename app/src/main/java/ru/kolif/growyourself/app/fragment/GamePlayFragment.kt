package ru.kolif.growyourself.app.fragment

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_game_play.*
import ru.kolif.growyourself.R
import ru.kolif.growyourself.app.activity.MainActivity
import ru.kolif.growyourself.core.util.MyOnTouch

open class GamePlayFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_game_play, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(buttonPlayStop)

        imageView.setOnTouchListener(MyOnTouch().touchListener)
        button_garden_tools.setOnClickListener {
            addFragment(InventoryFragment.newInstance())
        }
    }

    private fun addFragment(fragment: Fragment) {
        (activity as MainActivity).addFragment(fragment)
    }

    private fun initViews(v: View?) {
        val mediaPlayer = MediaPlayer.create(activity, R.raw.imagine_dragons_radioactive)
        buttonPlayStop.setOnClickListener {
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer.stop()
                mediaPlayer.reset()
            } else {
                mediaPlayer.start()
            }
        }
    }

    companion object {
        fun newInstance() = GamePlayFragment()
    }
}
