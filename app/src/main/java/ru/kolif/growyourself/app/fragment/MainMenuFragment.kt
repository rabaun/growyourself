package ru.kolif.growyourself.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main_menu.*
import ru.kolif.growyourself.R
import ru.kolif.growyourself.app.activity.MainActivity

open class MainMenuFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        main_menu_play.setOnClickListener {
            replaceFragment(GamePlayFragment.newInstance())
        }
    }

    companion object {
        fun newInstance() = MainMenuFragment()
    }

    private fun replaceFragment(fragment: Fragment) {
        (activity as MainActivity).replaceFragment(fragment)
    }

}