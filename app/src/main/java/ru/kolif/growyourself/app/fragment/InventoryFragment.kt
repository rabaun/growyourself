package ru.kolif.growyourself.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_inventory.*
import ru.kolif.growyourself.R
import ru.kolif.growyourself.app.activity.MainActivity

class InventoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_inventory, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_button.setOnClickListener {
            closeFragment()
        }
    }

    private fun closeFragment() {
        (activity as MainActivity).closeFragment()
    }

    companion object {
        fun newInstance() = InventoryFragment()
    }
}