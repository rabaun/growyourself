package ru.kolif.growyourself.app.activity

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import ru.kolif.growyourself.R
import ru.kolif.growyourself.app.fragment.MainMenuFragment
import ru.kolif.growyourself.core.extenstion.BaseActivity
import ru.kolif.growyourself.core.util.FragmentTransactor
import ru.kolif.growyourself.core.util.MediaPlayer11
import ru.kolif.growyourself.core.util.mediaPlayer

class MainActivity : BaseActivity() {

    private lateinit var fragmentTransactor: FragmentTransactor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragmentTransactor = FragmentTransactor(R.id.main_fragment_container)
        mediaPlayer = MediaPlayer.create(this, R.raw.imagine_dragons_radioactive)
        replaceFragment(MainMenuFragment.newInstance())
    }

    fun addFragment(fragment: Fragment) {
        fragmentTransactor.add(supportFragmentManager, fragment)
    }


    fun replaceFragment(fragment: Fragment) {
        fragmentTransactor.replace(supportFragmentManager, fragment)
    }

    fun closeFragment() {
        fragmentTransactor.pop(supportFragmentManager)
    }
}